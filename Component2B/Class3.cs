﻿using Component2B;
using Component2;
using ComponentFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2B
{
    public class Class3 : AbstractComponent,  Interface1
    {
        public void Metoda1()
        {
            Console.WriteLine("Metoda1 z Class3");
        }

        public void Metoda2()
        {
            Console.WriteLine("Metoda2 z Class3");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
