﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Component1;
using Component2;
using Component2B;
using ComponentFramework;

namespace MainB
{
    public class Class4
    {
        public void Metoda1()
        {
            Container kontener = new Container();
            Class3 c3 = new Class3();
            c3.RegisterProvidedInterface<Interface1>(c3);
            kontener.RegisterComponent(c3);

            Class1 c1 = new Class1(kontener.GetInterface<Interface1>());

            c1.Metoda5();
            Console.ReadKey();
        }
    }
}
