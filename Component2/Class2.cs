﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Component1;
using Component2;
using Component2B;


namespace Component2
{
    public class Class2 : AbstractComponent,  Interface1
    {
        public void Metoda1()
        {
            Console.WriteLine("Metoda1 z Class2");
        }

        public void Metoda2()
        {
            Console.WriteLine("Metoda2 z Class2");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
